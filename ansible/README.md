# Ansible Auftrag

## Erste Schritte

Der erste Schritt beim Benutzen von Ansible ist das Schreiben eines Inventories. Dort legen Nutzer fest, welche Hosts orchestriert werden sollen, darüber hinaus lassen sie sich zu Gruppen zusammenfassen. Inventories lassen sich entweder im YAML- oder im INI-Format verfassen.

Um zu testen ob alle Server verfügbar sind vom Inventory kann man "$ ansible -i inventory.yaml all -m ping" ausführen
Die Ausgabe dieses Kommandos könnte folgendermaßen aussehen:
**webserver1 | SUCCESS => {
    "changed": false,
    "ping": "pong"
}**
## Testing 

Das was ihr obendran gesehen habt ist ein Inventory test wenn ihr jetzt euer erstelltes yaml file testen wollt könnt ihr das auf verschiedene Arten in diesem Link sind verschiedene Möglichkeiten dies zu testen:

https://docs.ansible.com/ansible/latest/reference_appendices/test_strategies.html

## Inventory

Nachdem ihr Ansible installiert habt findet ihr im Verzeichnis etc/ansible 2 Files ansible.cfg und Hosts. Im Host File kommt wie der Name sagt eure Hosts rein im Gitlab habt ihr bereits ein Beispiel wie so ein Inventory aussieht. 

## Playbooks

Playbooks sind sogesagen die Anleitungen für die die Inventory Hosts das heisst ihr könnt im Playbook zum Beispiel sagen es soll einen Service neu starten und dann kannst du die Hosts angeben welche es betreffen sollte und es wird dann umgesetzt. 

