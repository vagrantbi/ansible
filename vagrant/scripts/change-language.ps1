# Title: Change Language
# Comments: This script is used to set the correct keyboard layout.
# Created by: Rajeevan Rabeendran (Noel Ballmer)
# Date: 29.01.2021
# Version: 0.2
# Update date: 15.02.2021
# Update from: Rajeevan Rabeendran
# Script to set the Windows settings
# Show LanguageList
$OldList = Get-WinUserLanguageList
# Add de-CH to LanguageList
$OldList.Add("de-CH")
Set-WinUserLanguageList -LanguageList $OldList -Force
Set-WinUserLanguageList -LanguageList de-CH -Force
