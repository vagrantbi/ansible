# Title: install telnet
# Comments: This script is to run commands on the vagrant VMs.
# Created by: Arno Siegenthaler
# Date: Unknown
# Version: 0.1
# Update date: -
# Update from: -

#Install telnet client
#Check if telnet is already installed
if ((Get-WindowsFeature -name Telnet-Client).InstallState -eq "Available") {
    Install-WindowsFeature -name Telnet-Client
}


