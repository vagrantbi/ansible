# Title: Install IIS
# Comments: This script is used to install iis.
# Created by: Arno Siegenthaler
# Date: Unknown
# Version: 0.1
# Update date: -
# Update from: -
#Script to install the IIS-Webserver on srv02

if ($(Get-WindowsFeature -Name Web-Server).InstallState -eq "Installed") {
    Write-Host "The IIS-Webserver is already installed. Therefore it will not be installed."
}
else {
    Write-Host "Installing IIS-Webserver, this could take a few minutes"
    Install-WindowsFeature -Name Web-Server
    Install-WindowsFeature -Name Web-Mgmt-Tools
}
