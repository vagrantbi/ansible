# Title: Join AD
# Comments: Script to Join the vagrant.lab domain
# Created by: Arno Siegenthaler
# Date: Unknown
# Version: 0.1
# Update date: -
# Update from: -

$domain = "vagrant.lab"
$user = "vagrant\Administrator"
$password = ConvertTo-SecureString -AsPlainText -Force -String "Welcome!20"
$adlogin = New-Object -TypeName System.Management.Automation.PSCredential -ArgumentList $user, $password
Write-Host "Joining domain $domain, please wait..."
try {
    Add-Computer -DomainName $domain -Credential $adlogin -ErrorAction Stop
}
catch {
    Write-Host "Could not join domain $domain!! Please check for errors."
}
