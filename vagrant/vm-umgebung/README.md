# Windows- und Linux Umgebung

## Starten der Umgebung
1. Starte ein Terminal (CMD oder Powershell) und navigiere mittels dem Befehl `cd` an den Ort, wo du das Repository gespeichert hast. Beispiel: `cd Desktop\vagrant-umgebung-la\windows-linux`
2. Überpüfe mittels dem Befehl `dir` ob du eine Datei namens `Vagrantfile` siehst. Ist dies der Fall, kannst du mit dem nächsten Schritt fortfahren.
3. Starte nun mittels `vagrant up` die Umgebung. Dies kann je nach Internetgeschwindigkeit und Leistung deines Gerätes bis zu 90 Minuten dauern. Du kannst aber gut etwas anderes während dieser Zeit machen. 
4. Wenn du wieder in der Konsole gelandet bist, kannst du dich mittels `vagrant rdp srv01` auf den Domain Controller verbinden. Mit `vagrant ssh srv-ubuntu01` kommst du auf den Ubuntu-Server, mit `vagrant ssh srv-fedora01` auf den Fedora-Server, mit `vagrant rdp cli-win01` auf den Windows-Client und mit `vagrant rdp cli-ubuntu01` auf den Ubuntu-Client. 

### Login
Falls du eine Aufforderung von Windows für das Login via RDP erhalten solltest, kannst du dich mit dem Benutzername `Administrator` oder `vagrant` und mit dem Passwort `vagrant` einloggen.

5. Wenn du deine Umgebung herunterfahren willst, so kannst du im Terminal `vagrant halt` eingeben. Dies fährt deine Maschinen sicher herunter, so dass sie zu einem Späteren Zeitpunkt mit `vagrant up` wieder gestartet werden können. Tipp: Du kannst auch mittels `vagrant up name-der-maschine` eine einzelne VM starten. Weitere Vagrant-Befehle findest du auf dem [Cheatsheet](#3-vagrant-cheatsheet) unten auf dieser Seite.

## 2. Die Verschiedenen Maschinen im Überblick
In diesem Abschnitt findest du allerlei nützliche Tipps und Tricks rund um das Thema Vagrant im allgemeinen aber auch in dieser spezifische Umgebung. 
Momentan stehen dir in dieser Umgebung die folgenden fünf Maschinen zur Verfügung:

1. `srv01`: Dies ist der Domain Controller, auf dem ein Active Direcory sowie ein DNS-Server läuft. Auf dem Server läuft ein Windows Server 2019 Betriebssystem. Verbinden kannst du dich mit `vagrant rdp srv01`. Die Verbindung via Powershell funktioniert hier leider nicht. 
2. `srv-ubuntu01`: Auf disem Linux-Server läuft Ubuntu-Server 18.04. Der Server hat keine grafische Oberfläche, da diese auf Linux Servern eigentlich nie installiert wird. Du kannst mit `vagrant ssh srv-ubuntu01` aber trozdem auf den Server zugreifen. 
3. `srv-fedora01`: Dies ist der zweite Linux-Server. Auf ihm läuft die Serverversion von Fedora 32. Du kannst auch hier via SSH mit `vagrant ssh srv-fedora01` verbinden.
4. `cli-win01`: Dies ist ein Windows-Client auf welchem Windows 10 Version 2004 läuft. Du kannst dich mit `vagrant rdp cli-win01` oder `vagrant powershell cli-win01` verbinden. 
5. `cli-ubuntu01`: Dies ist ein Ubuntu-Client, auf welchem Ubuntu 20.04 mit grafischer Oberfläche läuft. Du kannst dich mit `vagrant rdp cli-ubuntu01` oder mit `vagrant ssh cli-ubuntu01` auf den Client verbinden. 

## 3. Vagrant-Cheatsheet
In diesem Abschnitt findest du allerlei nützliche Tipps und Tricks rund um diese Umgebung, aber auch rund um Vagrant im allgemeinen.

Die Vagrant-Syntax ist wie folgt aufgebaut: Zuerst kommt immer das Programm `vagrant` selber. Danach sagtst du was du machen willst, also z.B. `vagrant up` um alle Maschinen zu starten. Wenn du jetzt hinter einen Befehl aber noch den Maschinennamen angibst, also z.B. `vagrant up srv02`, so wird der Befehl nur für diese Maschine ausgeführt. Es gibt aber auch Befehle, wie `vagrant powershell` oder `vagrant rdp`, wo du den Maschinennamen angeben musst.

Wenn du nur `vagrant` eingibst, so siehst du eine Übersicht mit allen verfügbaren Befehlen. Mit `vagrant up --help` z.B. kannst du dir auch Hilfe zu einem bestimmten Befehl ausgeben lassen. 

- `vagrant halt`: Alle Maschinen herunterfahren
- `vagrant up`: Die Maschinen erneut starten
- `vagrant halt cli-ubuntu01`: Nur cli-ubuntu01 herunterfahren
- `vagrant reload`: Alle Maschinen neu starten
- `vagrant destroy srv01`: srv01 löschen, kann mit `vagrant up srv01` erneut erstellt und gestartet werden. **Achtung:** Dabei gehen alle Daten, welche auf der VM gespeichert wurden verloren!
- `vagrant destroy -f`: Alle Maschinen ohne Bestätigung löschen. **Achtung:** Dabei gehen alle Daten, welche auf der VM gespeichert wurden verloren!
- `vagrant provision`: Die Installationsscripts noch einmal ausführen.
- Weitere nützliche Tipps und Tricks findest du in der offiziellen Dokumentation von [Vagrant](https://www.vagrantup.com/docs/cli) (auf Englisch).
